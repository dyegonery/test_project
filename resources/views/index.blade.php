<!DOCTYPE html>
<html ng-app="testApp">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Test Project - STC</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jq-2.2.4/dt-1.10.13/datatables.min.css"/>
        <link rel="stylesheet" href="plugins/angular-datatables/datatables.bootstrap.min.css">

        <link href="css/app.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="plugins/ladda-bootstrap/ladda-themeless.min.css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jq-2.2.4/dt-1.10.13/datatables.min.js"></script>

        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.3/angular.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.3/angular-route.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.3/angular-resource.js"></script>
        <script src="plugins/angular-datatables/angular-datatables.min.js"></script>


        <script src="js/app.js?v=2"></script>

        <script src="plugins/ladda-bootstrap/spin.min.js"></script>
        <script src="plugins/ladda-bootstrap/ladda.min.js"></script>

        <script src="app/services/services.js?v=2"></script>
        <script src="app/controllers/mainCtrl.js?v=2"></script>
        <script src="app/controllers/authCtrl.js?v=2"></script>
        <script src="app/controllers/dashCtrl.js?v=2"></script>
        <script src="app/controllers/contactsCtrl.js?v=2"></script>
        <script src="app/app.js"></script>
        <script async defer src="https://apis.google.com/js/api.js" onload="this.onload=function(){};handleGoogleLoad()" onreadystatechange="if (this.readyState === 'complete') this.onload()"></script>
        <script type="text/javascript" src="//platform.linkedin.com/in.js">
            api_key: 86o8p1d86kunbp
            onLoad: handleLinkedinLoad
            lang: pt_BR
            scope: r_basicprofile r_emailaddress
        </script>
    </head>
    <body ng-controller="MainController" ng-init="load()">
        <div ng-view></div>
    </body>
</html>
