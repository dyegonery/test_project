<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'fk_user', 'fone'
    ];

    public static function store($data)
    {
        $model = new Contact($data);
        $model->save();

        return $model;
    }

    public static function remove($fk_contact)
    {
        $model = Contact::find($fk_contact);

        if ($model)
            $model->delete();

        return true;
    }
}
