<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Resolvers\SocialUserResolver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(SocialUserResolverInterface::class, SocialUserResolver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
