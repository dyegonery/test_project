<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialAccount extends Model {

	protected $table = 'social_accounts';

	public $timestamps = false;

	public $dates = ['expires_at'];

	protected $fillable = [
		'fk_user', 'provider', 'provider_id', 'token', 'expires_at'
	];

}
