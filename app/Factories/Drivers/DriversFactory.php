<?php

namespace App\Factories\Drivers;

class DriversFactory {

	public static function create($driver)
	{
		switch ($driver) {
			case 'linkedin':
				$class = new LinkedinDriver();
				break;
			case 'google':
				$class = new GoogleDriver();
				break;
			default: 
				$class = false;
				break;
		}

		return $class;
	}

}
