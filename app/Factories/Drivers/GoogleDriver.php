<?php

namespace App\Factories\Drivers;

use App\User;

class GoogleDriver implements DriversInterface {

	protected $provider = 'google';

	public function saveUser($object)
	{
		$arr = [
			'name' => $object->name,
			'email' => $object->email,
			'provider' => $this->provider,
			'provider_id' => $object->id,
			'photo' => $object->image
		];

		$user = User::where('email', $arr['email'])
			->first();

		if (!$user)
			$user = User::store($arr);
		else
			$user->addProvider(['provider' => $this->provider, 'provider_id' => $object->id]);

		return $user;
	}

	public function getToken($user, $token = null)
	{
		return $token;
	}

	public function verifyToken($token)
	{
		$client = new \Google_Client(['client_id' => env('GOOGLE_CLIENT_ID')]);
		$payload = $client->verifyIdToken($token);

		if ($payload)
			return true;
		else
			return false;
	}

}
