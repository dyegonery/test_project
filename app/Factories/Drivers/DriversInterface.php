<?php

namespace App\Factories\Drivers;

interface DriversInterface {

	public function saveUser($object);

	public function verifyToken($token);

	public function getToken($user, $token);

}
