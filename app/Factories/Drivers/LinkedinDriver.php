<?php

namespace App\Factories\Drivers;

use App\User;
use Carbon\Carbon;
use App\SocialAccount;

class LinkedinDriver implements DriversInterface {

	protected $provider = 'linkedin';

	public function saveUser($object)
	{
		$arr = [
			'name' => $object->name,
			'email' => $object->email,
			'provider' => $this->provider,
			'provider_id' => $object->id,
			'photo' => $object->image
		];

		$user = User::where('email', $arr['email'])
			->first();

		if (!$user)
			$user = User::store($arr);
		else
			$user->addProvider(['provider' => $this->provider, 'provider_id' => $object->id, 'token' => str_random(80), 'expires_at' => Carbon::now()->addMinutes(60)]);

		return $user;
	}

	public function getToken($user, $token = null)
	{
		if (is_null($token)) {
			$social_account = $user->social_accounts()->where('provider', $this->provider)
				->first();

			return $social_account->token;
		} else
			return $token;
	}

	public function verifyToken($token)
	{
		$social_account = SocialAccount::where('token', $token)
			->first();

		if ($social_account)
			return true;
		else
			return false;
	}

}
