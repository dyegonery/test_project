<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

use App\Factories\Drivers\DriversFactory;

class ProviderAuth
{

	public function handle($request, Closure $next, $guard = null)
	{
		$provider = $request->input('provider');
		$token = $request->input('token');

		$class = DriversFactory::create($provider);
		if ($class)
			$check_token = $class->verifyToken($token);
		else
			$check_token = false;

		if (!$check_token)
			return response('Unauthorized.', 401);

		return $next($request);
	}
}
