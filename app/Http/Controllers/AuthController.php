<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factories\Drivers\DriversFactory;

use Auth;
use App\User;

class AuthController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function authenticate(Request $request)
    {
        $class = DriversFactory::create($request->input('provider'));
        if ($class)
            $user = $class->saveUser((Object)$request->all());
        else
            return [
                'response' => false,
                'error' => 'Provider not found'
            ];

        $token = $class->getToken($user, $request->input('token'));

        return [
            'response' => true,
            'user' => $user,
            'token' => $token
        ];
    }

    public function verifyAuth(Request $request)
    {
        $class = DriversFactory::create($request->input('provider'));
        if ($class)
            $token = $class->verifyToken($request->input('token'));
        else {
            return ['response' => false];
        }


        return [
            'response' => $token,
        ];
    }

}
