<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factories\Drivers\DriversFactory;

use App\User;
use App\Contact;

class ApiController extends Controller
{

    public function getContacts(Request $request)
    {
        $path = storage_path() . "/app/public/data.json";
        $json = json_decode(file_get_contents($path), true);

        foreach ($json as $key => $item) {
        	$json[$key]['db_stored'] = false;
        	$json[$key]['checked']  = false;
			$contact = Contact::where('email', $item['email'])->first();

			if ($contact)
				$json[$key]['db_stored'] = true;
        }

        return $json;
    }

    public function getStoredContacts(Request $request)
    {
        $contacts = Contact::where('fk_user', $request->input('fk_user'))
            ->get();

        $contacts->each(function ($el) {
            $el->db_stored = true;
            $el->checked = false;
        });

        return [
            'response' => true,
            'data' => $contacts
        ];
    }

    public function storeContacts(Request $request)
    {
    	$arr = [
			'name' => $request->input('name'),
			'email' => $request->input('email'),
			'phone' => $request->input('phone'),
			'fk_user' => $request->input('fk_user')
		];

		$contact = Contact::where('email', $arr['email'])
			->first();

		if (!$contact)
			$contact = Contact::store($arr);

		return [
			'response' => true,
		];
    }

    public function storeContactsBatch(Request $request)
    {
    	$list = $request->input('items');

    	foreach ($list as $contact) {
    		$arr = [
				'name' => $contact['name'],
				'email' => $contact['email'],
				'phone' => $contact['phone'],
				'fk_user' => $request->input('fk_user')
			];

			$contact = Contact::where('email', $arr['email'])
				->first();

			if (!$contact)
				$contact = Contact::store($arr);
    	}

    	return [
    		'response' => true,
    	];
    }


    public function removeContacts(Request $request)
    {
		$contact = Contact::where('email', $request->input('email'));

		$data = $contact->first();

		if ($data)
			$contact->delete();

		return [
			'response' => true,
		];
    }

}
