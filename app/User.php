<?php

namespace App;

use Laravel\Passport\HasApiTokens;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'provider', 'provider_id', 'photo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function social_accounts()
    {
        return $this->hasMany('App\SocialAccount', 'fk_user');
    }


    public static function store($data)
    {
        $model = new User($data);

        $model->save();

        return $model;
    }


    public function addProvider($data)
    {
        $has_provider = SocialAccount::where('provider', $data['provider'])
            ->where('provider_id', $data['provider_id'])
            ->first();

        if ($has_provider)
            return $has_provider;
        else {
            $model = new SocialAccount();
            $model->provider = $data['provider'];
            $model->provider_id = $data['provider_id'];
            $model->fk_user = $this->id;

            if (isset($data['token'])) {
                $model->token = $data['token'];
                $model->expires_at = $data['expires_at'];
            }

            $model->save();

            return $model;
        }
    }
}
