## About Test Project

This test Project was made with the following components:

- Laravel 5.4.
- Angular 1.6.2.
- Angular DataTables.
- Laravel Passport.
- Sass.

Some of the requested features aren't completed yet. But, to make it more complete I already created some methods of the controllers that serve the API. Unfortunately, some features are missing. See below some details about the Project.

## Factory Pattern to decouple authentication

The use of Factory Pattern help decouple code of authentication and contacts search from different drivers (Linkedin and Google, for now). Explore the folder app/Factories to better understanding. All drivers have the same final operations (signin and import contacts), but the implementation of this same logic may be different between multiples drivers.

## Problems with contacts importation

Linkedin don't provide list of connectios easily. Aparently, it is necessary to make a request explaining better the use of the data, before getting the approval from them. With Google, the friends importation appears to be easier, but my lack of experience with SPA put me in trouble, since I wasn't able to make the request without refreshing the page. This way, I adapted this feature to a "much more easier" solution, to have something to present. I made the importation through a json file, located at storage/public/data.json.

Besides that, the importation also would work under the Factory Pattern. As previous said, the implementation of the same logic (importing contacts) cna be very different between the drivers. So, I created a factory that redirect the requests to each class, depending on the driver used at the moment. This makes easier to mantain the code on the future, in my opinion.


-- To show users in the table with some custom features I used the module Angular datatables, that provides a set of functionalities on the table.

## Features not completed

	-- Add to databases, deleting from importation;
	-- Importation of contacts from Google;
	