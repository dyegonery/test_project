angular.module('Services', [])
	.factory('$localStorage', ['$window', function($window) {
		return {
			set: function(key, value) {
				$window.localStorage[key] = value;
			},
			get: function(key, defaultValue) {
				return $window.localStorage[key] || false;
			},
			setObject: function(key, value) {
				$window.localStorage[key] = JSON.stringify(value);
			},
			getObject: function(key) {
				if($window.localStorage[key] != undefined){
					return JSON.parse( $window.localStorage[key] || false );
				}
				return false;   
			},
			remove: function(key){
				$window.localStorage.removeItem(key);
			},
			clear: function(){
				$window.localStorage.clear();
			},
		}
	}])

	.factory('SocialService', function ($http, $rootScope, $localStorage) {
		var obj = {
			signIn: function (data) {
				return $http.post('api/auth', data).then(function success (response) {
					if (response.data.response) {
						$rootScope.user = response.data.user;
						$rootScope.token = response.data.token;
						$rootScope.social_provider = data.provider;

						$localStorage.setObject('user', $rootScope.user);
						$localStorage.set('token', $rootScope.token);
						$localStorage.set('social_provider', $rootScope.social_provider);

						return true;
					} else
						return false;
				}, function error (resopnse) {
					alert("I'm failing. :(");
				});
			},

			logout: function () {
				switch ($rootScope.social_provider) {
					case 'google':
						gapi.auth2.getAuthInstance().signOut();
						break;
					case 'linkedin':
						IN.User.logout();
						break;
					default:
						break;
				}

				delete $rootScope.user;
				delete $rootScope.token;
				delete $rootScope.social_provider;
				$localStorage.clear();

				return true;
			},

			checkUser: function ()
			{
				var obj = {
					'token': $localStorage.get('token'),
					'provider': $localStorage.get('social_provider')
				};
				return $http.post('api/check_auth', obj).then(function (response) {
					return response.data;
				});
			}
		};


		return obj;
	})

	.factory('ContactService', function ($http, $rootScope) {

		var obj = {
			get_contacts: function (user) {
				return $http.post('api/contacts', {id: user.id, token: $rootScope.token, provider: $rootScope.social_provider}).then(function (response) {
					return response.data;
				}, function (error) {
					alert('Sorry. Something went wrong.');
				});
			},

			get_stored_contacts: function () {
				if ($rootScope.user)
					var fk_user = $rootScope.user.id;
				else
					var fk_user = null;

				return $http.post('api/stored_contacts', {fk_user: fk_user, token: $rootScope.token, provider: $rootScope.social_provider}).then(function (response) {
					return response.data;
				}, function (error) {
					alert('Sorry. Something went wrong.');
				});
			},

			db_store: function (obj) {
				obj.token = $rootScope.token;
				obj.provider = $rootScope.social_provider;
				return $http.post('api/store_contacts', obj).then(function (response) {
					return response.data;
				}, function (error) {
					alert('Sorry. Database store failed!');
				});
			},

			db_remove: function (email) {
				return $http.post('api/remove_contacts', {email: email, token: $rootScope.token, provider: $rootScope.social_provider}).then(function (response) {
					return response.data;
				}, function (error) {
					alert('Sorry. Something went wrong.');
				});
			},

			db_store_batch: function (arr) {
				return $http.post('api/store_contacts_batch', {fk_user: $rootScope.user.id, items: arr, token: $rootScope.token, provider: $rootScope.social_provider}).then(function (response) {
					return response.data;
				}, function (error) {
					alert('Sorry. Database store failed!');
				});
			}
		};

		return obj;

	})

	.factory('httpMutator', function ($rootScope) {
		return {
			request: function (config) {
				// if (($rootScope.token && $rootScope.social_provider) && (config.url.indexOf('api/') > -1))
				// 	config.url = URI(config.url).addSearch({'token': $rootScope.token, 'provider': $rootScope.social_provider}).toString();

				return config;
			}
		};
	});
