var app = angular.module('testApp', ['mainCtrl', 'authCtrl', 'dashCtrl', 'contactsCtrl', 'ngRoute', 'Services', 'ngResource']);

app.config(function ($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl: 'app/pages/home.html',
			controller: 'MainController'
		})

		.when('/auth', {
			templateUrl: 'app/pages/auth.html',
			controller: 'AuthController'
		})

		.when('/dashboard', {
			templateUrl: 'app/pages/dashboard.html',
			controller: 'DashboardController'
		})

		.when('/contacts', {
			templateUrl: 'app/pages/contacts.html',
			controller: 'ContactsController'
		})
});

app.config(function ($httpProvider) {
	$httpProvider.interceptors.push('httpMutator');
});

app.run(function ($rootScope, $localStorage) {
	if ($localStorage.getObject('user'))
		$rootScope.user = $localStorage.getObject('user');

	if ($localStorage.get('token'))
		$rootScope.token = $localStorage.get('token');

	if ($localStorage.get('social_provider'))
		$rootScope.social_provider = $localStorage.get('social_provider');
});

function handleGoogleLoad()
{
	var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/people/v1/rest"];
	gapi.load('client:auth2', function() {
        gapi.auth2.init({
			discoveryDocs: DISCOVERY_DOCS,
			clientId: '363919463189-skdm7qjgnuit2hub8pedf5sqgblfnqr6.apps.googleusercontent.com',
			scope: "https://www.googleapis.com/auth/contacts.readonly https://www.googleapis.com/auth/userinfo.profile"
        });
	});
}

function handleLinkedinLoad(){}
