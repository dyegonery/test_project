angular.module('authCtrl', [])
	.controller('AuthController', function ($scope, $http, $location, $rootScope, $localStorage, SocialService) {

		$scope.signin = function (provider) {
			switch (provider) {
				case 'google':
					var google_auth = gapi.auth2.getAuthInstance();
					google_auth.signIn().then(function (e) {
						var cur_user = google_auth.currentUser.get();
						var profile = cur_user.getBasicProfile();
						var auth_response = cur_user.getAuthResponse();
						var token = auth_response.id_token;

						var data = {
							id: profile.getId(),
							name: profile.getName(),
							email: profile.getEmail(),
							image: profile.getImageUrl(),
							token: token,
							provider: 'google'
						};
						SocialService.signIn(data).then(function (success) {
							if (success)
								$location.path('/dashboard');
						});
					});

					break;
				case 'linkedin':
					IN.User.authorize(function(){
						IN.API.Profile("me").fields("id", "first-name", "last-name", "email-address", "picture-url").result(function (result) {
							var ln_user = result.values[0];
							var data = {
								id: ln_user.id,
								name: ln_user.firstName + ' ' + ln_user.lastName,
								email: ln_user.emailAddress,
								image: ln_user.pictureUrl,
								token: '',
								provider: 'linkedin'
							};

							SocialService.signIn(data).then(function (success) {
								if (success)
									$location.path('/dashboard');
							});
						});
					});
					break;
				default:
					break;
			}
		}

		$rootScope.logout = function()
		{
			SocialService.logout();

			$location.path('/');
		}
		
	});
