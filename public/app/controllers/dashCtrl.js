angular.module('dashCtrl', ['datatables'])
	.controller('DashboardController', function ($scope, $http, $rootScope, DTOptionsBuilder, ContactService, SocialService, $location, $localStorage) {

		SocialService.checkUser().then(function (data) {
			if (data.response !== true) {
				SocialService.logout();
				$location.path('/auth');
			}
		});

		if ($localStorage.getObject('contacts'))
			$scope.btn_clicked = 1;
		else
			$scope.btn_clicked = 0;

		$scope.contacts = $localStorage.getObject('contacts') || [];
		$scope.checked_contacts = [];

		$scope.dtOptions = DTOptionsBuilder
			.newOptions()
			.withOption('hasBootstrap', true);

		$scope.get_contacts = function () {
			var l = Ladda.create(document.querySelector( '#btn_import' ) );
			l.start();
			switch ($rootScope.social_provider) {
				case 'google':
					gapi.client.load('https://people.googleapis.com/$discovery/rest', 'v3', function () {
						gapi.client.people.people.connections.list({
							'resourceName': 'people/me',
							'requestMask.includeField': ['person.phoneNumbers', 'person.names', 'person.email_addresses'],
							'sortOrder' : 'FIRST_NAME_ASCENDING',
							'pageSize': 500,
						}).then(function (response) {
							var connections = response.result.connections;
							var arr_contacts = [];
							for (var i = 0; i < connections.length; i++) {
								var person = connections[i];
								if (typeof person.emailAddresses != 'undefined' && typeof person.names != 'undefined') {
									var obj = {
										id: i++,
										name: person.names[0].displayName,
										email: person.emailAddresses[0].value,
										checked: false,
										db_stored: false,
										phone: ''
									};

									if (typeof person.phoneNumbers != 'undefined') {
										obj.phone = person.phoneNumbers[0].value;
									}

									arr_contacts.push(obj);
								}
							}
							$localStorage.setObject('contacts', arr_contacts);
							$scope.$apply(function () {
								$scope.contacts = angular.copy(arr_contacts);
								$scope.btn_clicked = 1;
							});
							l.stop();
						});
					});
					break;
				case 'linkedin':
					ContactService.get_contacts($rootScope.user).then(function (data) {
						$scope.contacts = data;
						$localStorage.setObject('contacts', data);
						$scope.btn_clicked = 1;
						l.stop();
					});
					break;
				default:
					break;
			}

			return true;
		}

		$scope.db_store = function (key) {
			var obj = $scope.contacts[key];

			obj.fk_user = $rootScope.user.id;
			ContactService.db_store(obj).then(function (data) {
				if (data.response == true) {
					$scope.contacts[key].db_stored = true;
					$localStorage.setObject('contacts', $scope.contacts);
				}
			});
		}

		$scope.db_remove = function (key) {
			var obj = $scope.contacts[key];

			ContactService.db_remove(obj.email).then(function (data) {
				if (data.response == true) {
					$scope.contacts[key].db_stored = false;
					$localStorage.setObject('contacts', $scope.contacts);
				}
			});
		}

		$scope.check_contact = function (key) {
			var obj = $scope.contacts[key];
			var index = $scope.checked_contacts.indexOf(key);

			if (!obj.checked) {
				$scope.checked_contacts.splice(index, 1);
			} else if (index === -1)
				$scope.checked_contacts.push(key);
		}

		$scope.db_store_checked = function () {
			var arr_objs = [];
			for (var i = 0; i <= $scope.checked_contacts.length - 1; i++) {
				arr_objs.push($scope.contacts[$scope.checked_contacts[i]]);
			}

			ContactService.db_store_batch(arr_objs).then(function (data) {
				if (data.response == true) {
					for (var j = 0; j <= $scope.checked_contacts.length - 1; j++) {
						$scope.contacts[$scope.checked_contacts[j]].db_stored = true;
					}
					$localStorage.setObject('contacts', $scope.contacts);
				}
			});
		}
	});
