angular.module('contactsCtrl', ['datatables'])
	.controller('ContactsController', function ($scope, SocialService, ContactService, $rootScope, DTOptionsBuilder, $location) {

		SocialService.checkUser().then(function (data) {
			if (data.response !== true) {
				SocialService.logout();
				$location.path('/auth');
			}
		});

		$scope.db_contacts = [];
		$scope.checked_contacts = [];

		$scope.dtOptions = DTOptionsBuilder
			.newOptions()
			.withOption('hasBootstrap', true);

		ContactService.get_stored_contacts().then(function (result) {
			if (result.response == true) {
				$scope.db_contacts = result.data;
			}
		});

		$scope.db_remove = function (key) {
			var contact = $scope.db_contacts[key];
			ContactService.db_remove(contact.email).then(function (data) {
				if (data.response == true)
					$scope.db_contacts.splice(key, 1);
			});
		}

	});
