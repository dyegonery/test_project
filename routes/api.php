<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/check_auth', 'AuthController@verifyAuth');
Route::post('/auth', 'AuthController@authenticate');
Route::post('/contacts', 'ApiController@getContacts')->middleware('auth.social');
Route::post('/stored_contacts', 'ApiController@getStoredContacts')->middleware('auth.social');
Route::post('/store_contacts', 'ApiController@storeContacts')->middleware('auth.social');
Route::post('/remove_contacts', 'ApiController@removeContacts')->middleware('auth.social');
Route::post('/store_contacts_batch', 'ApiController@storeContactsBatch')->middleware('auth.social');
